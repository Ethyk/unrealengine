All Session Nodes are Asynchronous tasks (Clock Symbol) and will call “OnSuccess” or “OnFailure” once they're finished. In the meanwhile, you can use the topmost exec output.

For implementation Sessions in C++, please check the next chapter.

## Creating Sessions

For Players to find a Session and potentially join it, you need to create a Session and set up its properties, as well as decide which of those properties will be visible. Registering a Dedicated Server Session via Blueprint might not be supported in the current Engine Version. In C++ you should do the normal C++ Session creation inside the AGameSession by overriding the 'RegisterServer' function!

### Creating a Session via Blueprint

![[create_session-13c23a.png]]

  

For creating a Session in Blueprints, you can use the 'CreateSession' Node that Epic already exposed. It doesn't offer many options, but this can be extended via Plugins from the Forum.

## Updating Sessions

Updating is done when you want to change the Settings of an existing Session and is performed using the 'IOnlineSession::UpdateSession()' function.

For example, the Session may be set up currently to only allow 8 Players, while it needs to allow 12 Players for the next match. To update the Session, 'UpdateSession()' would be called passing it new Session Settings that specify a 12 player maximum.

When the request to update a Session has been completed, the 'OnUpdateSessionComplete' delegate is fired. This provides the opportunity to perform any configuration or initialization necessary to handle the Session Settings changing.

Updating a Session is generally performed between Matches on the Server, but it is also performed on the Client to keep the Session information in sync.

### Updating a Session via Blueprint

There is no Blueprint Version of this function yet, but it can be extended via Plugins from the Forum.

## Destroying Sessions

When a Session ends and is no longer needed, the Session is destroyed using the 'IOnlineSession::DestroySession()' function. When the destruction operation has been completed, the 'OnDestroySessionComplete' Delegate is fired, enabling you to perform cleanup operations.

### Destroying a Session via Blueprint

![[destroy_session-8ce60b.png]]

## Searching Sessions

The simplest way to find Sessions is to search for Sessions that match some desired subset of Settings.

This could be in response to the Player choosing a collection of filters in a User Interface, or it could be done automatically behind the scenes, based on the Player's skill and other factors, or it could be a combination of both methods.

The most basic form of Searching for Sessions is the classic Server Browser that shows all available games and allows the Player to filter those based on the type of Game they want to play.

### Searching Sessions via Blueprint

![[find_sessions-f54ccc.png]]

For Blueprints you can use the 'FindSessions' Node that Epic already exposed for you. You can specify the number of results and if you want to search for LAN or Online Games.

More can be specified using Plugins from the Forum.

## Joining Sessions

Once you have determined a Session for the Player to join, the process of joining is initiated by calling 'IOnlineSession::JoinSession()' and passing it the Number of the Player and the Name and SearchResult of the Session to join.

When the joining process completes, the 'OnJoinSessionComplete' Delegate is fired. This is where the logic to get the Player into the Match is performed.

More information about this logic can be read below and on my Session C++ Blogpost.

### Joining a Session via Blueprint

![[join_session-42afd.png]]

  

For joining a Session via Blueprint, you can use the 'JoinSession' Node, that Epic already exposed. It needs a SessionResult, which you can get from the 'FindSession' Node.

It returns on Array of SessionResults. The 'JoinSession' Node will directly join the Map 'OnSuccess'.

You don't have to deal with that.

## Cloud-Based Matchmaking

Cloud-Based Matchmaking refers to built-in Matchmaking Services that are available, and generally platform specific.  
An example of this type of service is the TrueSkill System available through the Microsoft Xbox Live Service.

To initiate Matchmaking on Platforms that support it, you call 'IonlineSession::Startmatchmaking()' and pass it the Controller Number of the Player to matchmake for, the SessionName, SessionSettings to use if creating a new Session, and Settings to Search against. The 'OnMatchmakingComplete' Delegate is fired when Matchmaking is complete. This provides a bool specifying whether the process was successful and the Name of the Session to join in that case.

A Matchmaking action, which is in progress, can be canceled by calling 'IOnlineSession::CancelMatchmaking()' and passing it the Controller Number of the Player and the SessionName that was passed to the call to start Matchmaking in the first place.

The 'OnCancelMatchmakingCompletedelegate' is fired when the cancel operation is completed.

## Following and Inviting Friends

On platforms that support the concept of Friends, Players can follow Friends into a Session or invite Friends to join them in a Session.

Following a Friend into a Session is done by calling 'IOnlineSession::FindFriendSession()' and passing it the Number of the local Player that wants to join the Session and the ID of the Friend already in the Session.  
The 'OnFindFriendSessionComplete' Delegate is fired when the Session is found and it contains a SearchResult that can be used to join the Session.

A Player can also invite one or more Friends to join them in their current Session using 'IOnlineSession::SendSessionInviteToFriend()' or 'IOnlineSession::SendSessionInviteToFriends()' and passing in the Local Player Number, SessionName, and ID(s) of the Player(s) to be invited.

When a Friend accepts an invitation, the 'OnSessionInviteAccepted' Delegate containing the SearchResult of the Session to join is fired.
