
This post shall give you a short introduction to handling your Multiplayer Sessions via your own C++ code. Most of you probably either started with the very limited native Blueprint Nodes or fell back to using Plugins like the Advanced Sessions one.

The tutorial will utilize the OnlineSubsystem NULL. OnlineSubsystems, like Steam, should theoretically work with this too as it's all interface-based, however, I haven't tested it with Steam yet.

## Code Project

### Files / Repository

The full code for handling Sessions in C++ can be found in the following GitHub Repository.

[https://github.com/eXifreXi/CppSessions](https://github.com/eXifreXi/CppSessions)

### Report Problems

If you encounter issues with the posted code, please post those into the Issues tab of the GitHub Repo.

[https://github.com/eXifreXi/CppSessions/issues](https://github.com/eXifreXi/CppSessions/issues)

### Suggest Changes

If you wish to suggest changes, improvements, updates, and whatnot, please also utilize the GitHub Repo.

[https://github.com/eXifreXi/CppSessions/pulls](https://github.com/eXifreXi/CppSessions/pulls)

## Version information

The Tutorial was created with Unreal Engine 4.26.2 inside an empty C++ project.

## Getting started

### Preparing the Project

#### Create and Clean up new Project

One of the things I do when creating fresh C++ projects is clean up the Source code folder. Epic Games throws a GameMode at us, which is 'really' nice for an 'empty' project, so I delete that one straight away.

I'm also one of those peeps that utilize Public and Private folders in projects that are used by other people. Inside of those, I move the Game's Module .cpp and .h and then regenerate project files and recompile.

The final folder structure, before we start adding our code, will look like this. My project is called CppSessions, so I will prefix my classes with CS.

![[cppsessions-32fedws.png]]

#### Change your "DefaultEngine.ini" file

Locate the “DefaultEngine.ini” file in your project's config folder. Then add the following lines to it:

```
[OnlineSubsystem]
DefaultPlatformService=Null
```

This defines the Default OnlineSubsystem that UE4 should use. Since this tutorial only covers the usage of NULL, that's what we are going to utilize. For those of you who want to research this a bit more, you can find the variable in “OnlineSubsystemModule.h:26”.

As far as I am aware, this is not 100% required, as I have working Session code in Projects that don't have this set. But you'll ultimately need it if you want to move away from the NULL OnlineSubsystem.

#### Change your "\<Your Project Name>.Build.cs"

For my project, as you can see in the image further up, this would be “CppSessions.Build.cs”. Here we need to add two Modules as dependencies since we later want to access their exported functions and variables.

```
OnlineSubsystemOnlineSubsystemUtils
```

“OnlineSubsystem” will offer us all the Session related code. An OnlineSubsystem has a lot of other functionalities, such as User Data, Stats, Achievements, and much more. We are only really interested in the “SessionInterface” of it.

“OnlineSubsystemUtils” contains some helper functions, such as compacter ways of accessing specific Interfaces. There is an important note to make here: The function that we will use from the “OnlineSubsystemUtils”, especially the ones from its header, also exists similarly in the “Online.h” file. The problem with those however is, that they aren't taking the current World into account. That can lead to issues when trying Session Code in PIE (Play In Editor) because the Editor can have multiple different worlds. You may not find any Sessions for example.

Your final Build.cs file should look something like this:

```cpp
using UnrealBuildTool;

public class CppSessions : ModuleRules
{
	public CppSessions(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
		    "Core",
		    "CoreUObject",
		    "Engine",
		    "InputCore",
		    "OnlineSubsystem",
		    "OnlineSubsystemUtils",
		});
	}
}
```

And that's almost it. Now you are ready to add new classes and files to your project, which we will utilize to place our session-related code into. My original writeup is quite old and I was using the GameInstance class to hold the Session code. The reason behind that was and more or less still is, that you can easily access your session-related code from everywhere at any time. That includes executing Session related functions as well as accessing and modifying cached Session Settings.

We will however not place the code directly into the GameInstance anymore. Since 4.24 (I think), UE4 offers new Subsystems which are Framework related. They are not OnlineSubsystems, so try to not mix those up. These Subsystems share the lifetime of their “owner”. You can read up on them in the UE4 Documentation. One of those exists for the GameInstance, which allows us to neatly pack away our code but still utilize the persistent nature of the GameInstance class. Subsystems have static getter functions for Blueprints, so you should be able to easily access them from within your Blueprint Code.

### Add a GameInstance Subsystem class

As previously stated, my classes will be prefixed with CS, due to the project being called CppSessions.

![[create_gameinstance_subsystem_class.png]]

Use whatever method you want to create a new Class based on the “UGameInstanceSubsystem” Class. The final name of mine will be “CSSessionSubsystem”. I placed the file into a “Subsystems” subfolder to stay organized.

![[project_folder_hierarchy_with_subsystem_files.png]]

I will add the constructor declaration and definition to it, so we can later use it to bind our callback functions.

>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "CSSessionSubsystem.generated.h"

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();
};
```

>[!note] CSSessionSubsystem.cpp 
```cpp
#include "Subsystems/CSSessionSubsystem.h"

UCSSessionSubsystem::UCSSessionSubsystem()
{
}
```


## The Session Code

I won't go into detail on every posted code snippet, but rather point out specific parts of the code if needed. There is a high chance that you want to modify your code a bit later on. It might for example be a good idea to check if a Session is already existing before Creating or Joining a new one. You could then save what you were supposed to do and Destroy the Session first, before returning to Creating or Joining.

Here are all the Session Methods we will implement:

- Create Session
- Update Session
- Start Session
- End Session
- Destroy Session
- Find Sessions
- Join Session

>[!note] Note
>There are more. For example “Find Sessions” also exists for things like “Find a Session of a Friend”, but we won't tackle those today. They follow a similar setup and I'm sure you'll be able to set that up yourself whenever you need to.

The whole setup of each of these methods will follow the same concept:

- Function to execute your Session code
- Function to receive a callback from the OnlineSubsystem
- Delegate to bind your function to
- DelegateHandle to keep track of the Delegate and later unbind it again

Each Gist will only contain the Subsystem Class with the specific Method we are looking at. That means you'll see the CSSessionSubsystem.h/.cpp 7 times, with only the functions and variables relevant to the current Method. If you wish to see all 7 Session Methods implemented at once, head over to the public GitHub Repro that is linked at the start of this post.

### Create Session
>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnCreateSessionComplete, bool, Successful);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();

	void CreateSession(int32 NumPublicConnections, bool IsLANMatch);
	
	FCSOnCreateSessionComplete OnCreateSessionCompleteEvent;

protected:
	void OnCreateSessionCompleted(FName SessionName, bool Successful);

private:
	FOnCreateSessionCompleteDelegate CreateSessionCompleteDelegate;
	FDelegateHandle CreateSessionCompleteDelegateHandle;
	TSharedPtr<FOnlineSessionSettings> LastSessionSettings;
};
```

```cpp
#include "Interfaces/OnlineSessionInterface.h"
```

This is required to be able to use both the “FOnlineSessionSettings” struct and the “FOnCreateSessionCompleteDelegate” delegate. Same goes for future Delegates of other Methods.

>[!note] CSSessionSubsystem.cpp

```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: CreateSessionCompleteDelegate(FOnCreateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnCreateSessionCompleted))
{
}

void UCSSessionSubsystem::CreateSession(int32 NumPublicConnections, bool IsLANMatch)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnCreateSessionCompleteEvent.Broadcast(false);
		return;
	}

	LastSessionSettings = MakeShareable(new FOnlineSessionSettings());
	LastSessionSettings->NumPrivateConnections = 0;
	LastSessionSettings->NumPublicConnections = NumPublicConnections;
	LastSessionSettings->bAllowInvites = true;
	LastSessionSettings->bAllowJoinInProgress = true;
	LastSessionSettings->bAllowJoinViaPresence = true;
	LastSessionSettings->bAllowJoinViaPresenceFriendsOnly = true;
	LastSessionSettings->bIsDedicated = false;
	LastSessionSettings->bUsesPresence = true;
	LastSessionSettings->bIsLANMatch = IsLANMatch;
	LastSessionSettings->bShouldAdvertise = true;

	LastSessionSettings->Set(SETTING_MAPNAME, FString("Your Level Name"), EOnlineDataAdvertisementType::ViaOnlineService);

	CreateSessionCompleteDelegateHandle = sessionInterface->AddOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate);

	const ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	if (!sessionInterface->CreateSession(*localPlayer->GetPreferredUniqueNetId(), NAME_GameSession, *LastSessionSettings))
	{
		sessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegateHandle);

		OnCreateSessionCompleteEvent.Broadcast(false);
	}
}

void UCSSessionSubsystem::OnCreateSessionCompleted(FName SessionName, bool Successful)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (sessionInterface)
	{
		sessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegateHandle);
	}

	OnCreateSessionCompleteEvent.Broadcast(Successful);
}
```

Let's talk about a few things here which will be repeated throughout the next Methods.

```cpp
CreateSessionCompleteDelegate(FOnCreateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnCreateSessionCompleted))
```

This line will bind the Subsystems function for the CreateSessionComplete Callback to our Delegate. We will have to do that for each of the Callbacks.

```cpp
const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
```

When using “GetSessionInterface(..)”, always make sure to pass in the current UWorld. There are two versions of these helper functions. One is declared and defined in the Online.h file, not needing a UWorld, and one is declared and defined in the OnlineSubsysbtemUtils.h file, requiring the current UWorld. The difference is that the UWorld version takes into account that the Editor has multiple UWorlds at the same time.

Not providing the UWorld will not handle PIE (Play In Editor) properly, resulting in issues with not finding sessions and similar.

```cpp
CreateSessionCompleteDelegateHandle = sessionInterface->AddOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate);
```

Here we are simply adding our Delegate to the SessionInterfaces Delegate List, as well as saving the Handle that it returns, so we can later remove the Delegate from the List again.

```
NAME_GameSession
```

You'll see this a lot. Some Subsystems might support other Sessions, like PartySessions, but we won't look at those in this post. It describes the type of Session as a Game(play) one.

```cpp
IOnlineSubsyconst IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());if (sessionInterface){    sessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegateHandle);}
```

In the Callback Function, we will always clear the Callback (as well as when the actual function call fails).

```cpp
OnCreateSessionCompleteEvent.Broadcast(Successful);
```

Our custom Delegate is used to broadcast the event back to whoever called this. For example, a button press in your UMG Widget would bind to it, call CreateSession and then get the callback to react to it. You can also tie this into a Latent node, similar to how the Native Nodes work if you want to keep it compact. I won't show any Latent node code here though as the Engine has enough examples. None of the code is exposed to Blueprints of course, so you'll have to take care of that yourself.

```cpp
LastSessionSettings = MakeShareable(new FOnlineSessionSettings());
```

Now, you will notice a lot of LastSessionSettings stuff in the CreateSession function. What I used here isn't 100% needed, or the one thing you always have to use. Please read through the available Settings and decide on your own if you want them true/false or any other value.

The custom settings that are defined as followed can of course be passed in via the CreateSession function, but I didn't want to make the function signature that huge. A good idea is to wrap this stuff into a Struct, so you can easier pass it along.

```cpp
LastSessionSettings->Set(SETTING_MAPNAME, FString("Your Level Name"), EOnlineDataAdvertisementType::ViaOnlineService);
```

The “SETTING_MAPNAME” is defined in “OnlineSessionSettings.h:15”. There are a few more, but you aren't limited to those. You can simply add some definitions into the header file of your Subsystem or some custom header just for types.

### Update Session

Small note here: While I originally said, that we will look at each Method on its own, the UpdateSession Method works a lot better if you at least keep the original settings saved somewhere. So you'll see the “LastSessionSettings” pop up again here, but I will only modify them, not create them. This expects you to have the CreateSession part already added.

I also won't pass any actual changes as Inputs. You can do that of course. For simplicity, I will only adjust the MapName in this UpdateSession call.

>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnUpdateSessionComplete, bool, Successful);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();

	void UpdateSession();
	
	FCSOnUpdateSessionComplete OnUpdateSessionCompleteEvent;

protected:
	void OnUpdateSessionCompleted(FName SessionName, bool Successful);

private:
	TSharedPtr<FOnlineSessionSettings> LastSessionSettings;

	FOnUpdateSessionCompleteDelegate UpdateSessionCompleteDelegate;
  	FDelegateHandle UpdateSessionCompleteDelegateHandle;
};
```

>[!note] CSSessionSubsystem.cpp
```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: UpdateSessionCompleteDelegate(FOnUpdateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnUpdateSessionCompleted))
{
}

void UCSSessionSubsystem::UpdateSession()
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnUpdateSessionCompleteEvent.Broadcast(false);
		return;
	}

	TSharedPtr<FOnlineSessionSettings> updatedSessionSettings = MakeShareable(new FOnlineSessionSettings(*LastSessionSettings));
	updatedSessionSettings->Set(SETTING_MAPNAME, FString("Updated Level Name"), EOnlineDataAdvertisementType::ViaOnlineService);

	UpdateSessionCompleteDelegateHandle =
		sessionInterface->AddOnUpdateSessionCompleteDelegate_Handle(UpdateSessionCompleteDelegate);

	if (!sessionInterface->UpdateSession(NAME_GameSession, *updatedSessionSettings))
	{
		sessionInterface->ClearOnUpdateSessionCompleteDelegate_Handle(UpdateSessionCompleteDelegateHandle);

		OnUpdateSessionCompleteEvent.Broadcast(false);
	}
	else
	{
		LastSessionSettings = updatedSessionSettings;
	}
}

void UCSSessionSubsystem::OnUpdateSessionCompleted(FName SessionName, bool Successful)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (sessionInterface)
	{
		sessionInterface->ClearOnUpdateSessionCompleteDelegate_Handle(UpdateSessionCompleteDelegateHandle);
	}

	OnUpdateSessionCompleteEvent.Broadcast(Successful);
}
```


### Start Session

Usually, you want to start the Session right after creating it. For a more Matchmaking approach you could first create the Session, then let Players find it and register them, and then start the Session and the Match.

>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnStartSessionComplete, bool, Successful);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();

	void StartSession();
	
	FCSOnStartSessionComplete OnStartSessionCompleteEvent;

protected:
	void OnStartSessionCompleted(FName SessionName, bool Successful);

private:
	FOnStartSessionCompleteDelegate StartSessionCompleteDelegate;
	FDelegateHandle StartSessionCompleteDelegateHandle;
};
```


>[!note] CSSessionSubsystem.cpp
```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: StartSessionCompleteDelegate(FOnStartSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnStartSessionCompleted))
{
}

void UCSSessionSubsystem::StartSession()
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnStartSessionCompleteEvent.Broadcast(false);
		return;
	}

	StartSessionCompleteDelegateHandle =
		sessionInterface->AddOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegate);

	if (!sessionInterface->StartSession(NAME_GameSession))
	{
		sessionInterface->ClearOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegateHandle);

		OnStartSessionCompleteEvent.Broadcast(false);
	}
}

void UCSSessionSubsystem::OnStartSessionCompleted(FName SessionName, bool Successful)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (sessionInterface)
	{
		sessionInterface->ClearOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegateHandle);
	}

	OnStartSessionCompleteEvent.Broadcast(Successful);
}
```

### End Session

Same as starting, you can End a Session by hand. I don't think a lot of users ever do this, they mostly outright Destroy the Session, but since the Method exists I wanted to show this one too.

>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnEndSessionComplete, bool, Successful);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();
	
	void EndSession();
	
	FCSOnEndSessionComplete OnEndSessionCompleteEvent;

protected:
	void OnEndSessionCompleted(FName SessionName, bool Successful);

private:
	FOnEndSessionCompleteDelegate EndSessionCompleteDelegate;
	FDelegateHandle EndSessionCompleteDelegateHandle;
};
```


>[!note] CSSessionSubsystem.cpp
```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: EndSessionCompleteDelegate(FOnEndSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnEndSessionCompleted))
{
}

void UCSSessionSubsystem::EndSession()
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnEndSessionCompleteEvent.Broadcast(false);
		return;
	}

	EndSessionCompleteDelegateHandle =
		sessionInterface->AddOnEndSessionCompleteDelegate_Handle(EndSessionCompleteDelegate);

	if (!sessionInterface->EndSession(NAME_GameSession))
	{
		sessionInterface->ClearOnEndSessionCompleteDelegate_Handle(EndSessionCompleteDelegateHandle);

		OnEndSessionCompleteEvent.Broadcast(false);
	}
}

void UCSSessionSubsystem::OnEndSessionCompleted(FName SessionName, bool Successful)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());;
	if (sessionInterface)
	{
		sessionInterface->ClearOnEndSessionCompleteDelegate_Handle(EndSessionCompleteDelegateHandle);
	}

	OnEndSessionCompleteEvent.Broadcast(Successful);
}
```

### Destroy Session

Destroying a Session has to happen on both server and clients when they leave. You might find yourself in a situation where you left a Server, but you forgot to clean up the Session and trying to Create or Join a new one won't work anymore until restarting the Game/Editor.

At that point, you should think about Destroying the Session (if it exists already) before Creating or Joining. That way you can ensure that your Players aren't getting stuck.

Or you come up with a secure way of Destroying the Session whenever the Player is not supposed to be in one. For example when entering the Main Menu.


>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnDestroySessionComplete, bool, Successful);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();

	void DestroySession();
	
	FCSOnDestroySessionComplete OnDestroySessionCompleteEvent;

protected:
	void OnDestroySessionCompleted(FName SessionName, bool Successful);

private:
	FOnDestroySessionCompleteDelegate DestroySessionCompleteDelegate;
	FDelegateHandle DestroySessionCompleteDelegateHandle;
};
```

>[!note]  CSSessionSubsystem.cpp
```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: DestroySessionCompleteDelegate(FOnDestroySessionCompleteDelegate::CreateUObject(this, &ThisClass::OnDestroySessionCompleted))
{
}

void UCSSessionSubsystem::DestroySession()
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnDestroySessionCompleteEvent.Broadcast(false);
		return;
	}

	DestroySessionCompleteDelegateHandle =
		sessionInterface->AddOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegate);

	if (!sessionInterface->DestroySession(NAME_GameSession))
	{
		sessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegateHandle);

		OnDestroySessionCompleteEvent.Broadcast(false);
	}
}

void UCSSessionSubsystem::OnDestroySessionCompleted(FName SessionName, bool Successful)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (sessionInterface)
	{
		sessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegateHandle);
	}

	OnDestroySessionCompleteEvent.Broadcast(Successful);
}
```

### Find Sessions

Finding Sessions is a bit annoying, because to properly show the results via your UI, you either have to stay in C++ or set up some Blueprint Types and static Functions to extract all the information from the Session Result. Especially your custom Settings, like the MapName we added in the Create Session part.

Of course, I won't show how to do that here, because we are in C++ land here.

Usually, you would also want to pass in some Search Settings into your FindSession node, but again, I want to keep it simple, so you aren't bombarded by code you might not need.

>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FCSOnFindSessionsComplete, const TArray<FOnlineSessionSearchResult>& SessionResults, bool Successful);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();

	void FindSessions(int32 MaxSearchResults, bool IsLANQuery);
	
	FCSOnFindSessionsComplete OnFindSessionsCompleteEvent;

protected:
	void OnFindSessionsCompleted(bool Successful);

private:
	FOnFindSessionsCompleteDelegate FindSessionsCompleteDelegate;
	FDelegateHandle FindSessionsCompleteDelegateHandle;
	TSharedPtr<FOnlineSessionSearch> LastSessionSearch;
};
```

>[!note] CSSessionSubsystem.cpp
```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: FindSessionsCompleteDelegate(FOnFindSessionsCompleteDelegate::CreateUObject(this, &ThisClass::OnFindSessionsCompleted))
{
}

void UCSSessionSubsystem::FindSessions(int32 MaxSearchResults, bool IsLANQuery)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnFindSessionsCompleteEvent.Broadcast(TArray<FOnlineSessionSearchResult>(), false);
		return;
	}

	FindSessionsCompleteDelegateHandle =
		sessionInterface->AddOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegate);

	LastSessionSearch = MakeShareable(new FOnlineSessionSearch());
	LastSessionSearch->MaxSearchResults = MaxSearchResults;
	LastSessionSearch->bIsLanQuery = IsLANQuery;

	LastSessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

	const ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	if (!sessionInterface->FindSessions(*localPlayer->GetPreferredUniqueNetId(), LastSessionSearch.ToSharedRef()))
	{
		sessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegateHandle);

		OnFindSessionsCompleteEvent.Broadcast(TArray<FOnlineSessionSearchResult>(), false);
	}
}

void UCSSessionSubsystem::OnFindSessionsCompleted(bool Successful)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (sessionInterface)
	{
		sessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegateHandle);
	}

	if (LastSessionSearch->SearchResults.Num() <= 0)
	{
		OnFindSessionsCompleteEvent.Broadcast(TArray<FOnlineSessionSearchResult>(), Successful);
		return;
	}

	OnFindSessionsCompleteEvent.Broadcast(LastSessionSearch->SearchResults, Successful);
}
```

The only important line here is:

```cpp
LastSessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
```

This will make sure that we are searching for Presence Sessions, so Player-hosted Sessions instead of DedicatedServer Sessions.

Please also be aware that due to “FOnlineSessionSearchResult” not being a USTRUCT, we can use a DYNAMIC Multicast Delegate, as those need to have types that can be exposed to Blueprints. So if you want to expose this Delegate to Blueprints, you need to wrap the SearchResult struct into your own USTRUCT and utilize some Function Library to communicate with the inner SearchResult struct.

### Join Session

>[!note] CSSessionSubsystem.h
```cpp
#pragma once

#include "CoreMinimal.h"

#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "CSSessionSubsystem.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FCSOnJoinSessionComplete, EOnJoinSessionCompleteResult::Type Result);

UCLASS()
class UCSSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UCSSessionSubsystem();

	void JoinGameSession(const FOnlineSessionSearchResult& SessionResult);
	
	FCSOnJoinSessionComplete OnJoinGameSessionCompleteEvent;

protected:
	void OnJoinSessionCompleted(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	bool TryTravelToCurrentSession();

private:
	FOnJoinSessionCompleteDelegate JoinSessionCompleteDelegate;
	FDelegateHandle JoinSessionCompleteDelegateHandle;
};
```

>[!note] CSSessionSubsystem.cpp
```cpp
#include "Subsystems/CSSessionSubsystem.h"

#include "OnlineSubsystemUtils.h"

UCSSessionSubsystem::UCSSessionSubsystem()
	: JoinSessionCompleteDelegate(FOnJoinSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnJoinSessionCompleted))
{
}

void UCSSessionSubsystem::JoinGameSession(const FOnlineSessionSearchResult& SessionResult)
{
		
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		OnJoinGameSessionCompleteEvent.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
		return;
	}

	JoinSessionCompleteDelegateHandle =
		sessionInterface->AddOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegate);

	const ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	if (!sessionInterface->JoinSession(*localPlayer->GetPreferredUniqueNetId(), NAME_GameSession, SessionResult))
	{
		sessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegateHandle);

		OnJoinGameSessionCompleteEvent.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
	}
}

void UCSSessionSubsystem::OnJoinSessionCompleted(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (sessionInterface)
	{
		sessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegateHandle);
	}

	OnJoinGameSessionCompleteEvent.Broadcast(Result);
}

bool UCSSessionSubsystem::TryTravelToCurrentSession()
{
	const IOnlineSessionPtr sessionInterface = Online::GetSessionInterface(GetWorld());
	if (!sessionInterface.IsValid())
	{
		return false;
	}

	FString connectString;
	if (!sessionInterface->GetResolvedConnectString(NAME_GameSession, connectString))
	{
		return false;
	}

	APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	playerController->ClientTravel(connectString, TRAVEL_Absolute);
	return true;
}
```

Again, be aware that “EOnJoinSessionCompleteResult” can not be exposed to Blueprints, so you can’t make your own Callback Delegate BlueprintAssignable (or Dynamic to begin with) unless you make your own UENUM and convert back and forth between UE4s type and yours.

The same goes for the JoinSession function itself, as the “FOnlineSessionSearchResult” struct can’t be exposed. If you want to make the function BlueprintCallable, you’ll have to wrap the struct into your own USTRUCT as stated before.

In addition to the default Session code, you’ll also find a new function called “TryTravelToCurrentSession” in the above gist, which is for actually joining the Server behind that Session.