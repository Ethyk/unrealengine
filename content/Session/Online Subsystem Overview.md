The Online Subsystem and its Interfaces exist to provide a clear abstraction to common online functionality across the available set of platforms in a given environment.

Platforms in this context would refer to Steam, Xbox Live, Facebook, etc. Portability is one of the main goals.

**For what do I need a Subsystem?**

By default, you will use the 'SubsystemNULL'. This allows you to host LAN Sessions (so you can find Sessions via a Server List and join them in your LAN network) or join directly via IP.

What it doesn't allow you is to host such a Session on the Internet.

**Why?**

Because you have no **Master Server** that provides the Client with a list of Servers/Sessions.

Subsystems, like for example Steam, will allow you to also host Servers/Sessions that are visible over the Internet. This is important to understand!

You can also create your own Subsystem/Master Server, but that requires a lot of coding outside of Unreal Engine.

## Online Subsystem Module

## Basic Design

The base module Online Subsystem is responsible for regulating how platform-specific modules are defined and registered with the Engine. All access to a platform service will go through this module.

When loaded, this module will in turn try to load the default platform server module specified in the 'Engine.ini' - file:

```cpp
[OnlineSubsystem]DefaultPlatformService = <Default Platform Identifier>
```

If successful, this default online interface will be available via the static accessor when no parameter is specified.

```cpp
static IOnlineSubsystem* Get(const FName& SubsystemName = NAME_None);
```

Additional services are loaded on demand when called from this function with a proper identifier.

### Use of Delegates

Much like previous Unreal Engine Versions, the Online Subsystem will make heavy use of Delegates when calling functions with Asynchronous side effects.

It is important to respect the Delegates and wait for the proper Delegate to be called before calling functions further down the chain.

Failure to wait for an Asynchronous task can cause crashes and unexpected, undefined behavior. Waiting for Delegates is especially important during connectivity failures such as a cable pull or other disconnect events. The amount of time a task may take to finish may seem instantaneous in the ideal case, but can be upwards to almost a minute in the timeout case.

The Delegate interface is fairly straightforward, with each Delegate clearly defined at the top of each interface header. Every Delegate has an Add, Clear, and Trigger function. (Although Triggering delegates manually is discouraged).

Common practice is to Add() the Delegate right before calling the appropriate function, and then Clear() the Delegate from within itself.

### Interfaces

Not all platforms will implement all Interfaces and Game Code should plan accordingly.

#### Profile

Interface definition for the online services Profile services. Profile services are defined as anything related to a given User Profile and its associated metadata (Online Presence, Access Permissions, etc).

#### Friends

Interface definition for the online services Friends services. Friends services are anything related to the Maintenance of Friends and Friends Lists.

#### Sessions

Interface definition for the online services Session services.  
Session services are defined as anything related to managing a Session and its state.

#### Shared Cloud

Provides the Interface for sharing files already on the cloud (see User Cloud with other users).

#### User Cloud

Provides the Interface for per User Cloud file storage.

#### Leaderboards

Provides the Interface for accessing online Leaderboards.

#### Voice

Provides the Interface for Voice Communication over network while in the game.

#### Achievements

Provides the Interface for Reading/Writing/Unlocking Achievements.

#### External UI

Provides the Interface for accessing a given platform's external Interfaces if available.

### Session and Matchmaking

Matchmaking is the process of matching Players with Sessions. A Session is an Instance of the Game running on the Server with a given set of properties, which is either advertised, so that it can be found and joined by Players wanting to play the Game, or private, so only Players who are invited or notified of it in some way can join.

Picture an Online Game Lobby that lists all of the Games currently being played.

Each Game in the list is a Session or individual online match. Players are matched with Sessions either by searching or some other means and then join the Session to play the Match.

#### Basic Life-Time of a Session

- Create a new Session with desired Settings
- Wait for Players to request to join the Match
- Register Players who want to join
- Start the Session
- Play the Match
- End the Session
- Un-register the Players. Either:
    - Update the Session if you want to change the Type of Match and go back to waiting for Players to join
    - Destroy the Session

#### Session Interface

The Session Interface, IOnlineSession, provides platform-specific functionality for setting up the pieces behind the scenes that is necessary to perform Matchmaking as well as other methods of allowing Players to find and join online games.

This includes Session Management, finding Sessions through search or other means, as well as joining and leaving those Sessions.

The Session Interface is created and owned by the Online Subsystem. This means it only exists on the Server.

Only one Session Interface will ever exist at a time - the Session Interface for the platform the engine is currently running on.  
While the Session Interface performs all of the Session handlings, the Game doesn't generally interact directly with it.

Instead, the GameSession, AGameSession, acts as a game-specific wrapper around the Session Interface and the game code makes calls to it when it needs to interact with the Session.

The GameSession is created and owned by the GameMode, and also only exists on the Server when running an online game.

Each game can potentially have multiple GameSession types, but only one will ever be used at a time.

The most common case for a game having more than one type of GameSession is to add a type of GameSession for when the game is using a Dedicated Server.

#### Session Settings

The SessionSettings, defined by the 'FOnlineSessionSettingsclass', are a set of properties that determine the characteristics of the session.

In the base implementation, these are things:

- Number of Players allowed
- Is the Session advertised or private
- Is the Session a LAN match
- Is the Server dedicated or Player-hosted
- Are invites allowed
- Etc.

Using the Online Game Lobby Example, each of those games is a Session and has its own SessionSettings.  
For example, some Sessions may be Player versus Player (PvP) while others are Cooperative Multiplayer (Co-Op).

Different Sessions could be playing different maps or playlists, requiring different numbers of players, etc.