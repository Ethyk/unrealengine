---
title: Multiplayer Network
---

This compendium is meant to give you a good start into multiplayer programming for Unreal Engine.

[

## 📄️ Introduction

This Compendium should only be used with a base understanding of the Singleplayer Game Framework of Unreal Engine.

](https://ethyk.gitlab.io/unrealengine/multiplayer/1-Introduction)

[

## 📄️ Network in Unreal

Unreal Engine uses a standard Server-Client architecture. This means the server is authoritative and all data must be sent from the client to the server first. After which the server validates the data and reacts depending on your code.

](https://ethyk.gitlab.io/unrealengine/multiplayer/2-network-in-unreal)

[

## 🗃️ Gameplay Framework

7 items

](https://ethyk.gitlab.io/unrealengine/multiplayer/3-gameplay-framework)

[

## 📄️ Gameplay Framework + Network

With the previous information about Unreal Engine's Server-Client architecture and the common classes we can divide those into four categories:

](https://ethyk.gitlab.io/unrealengine/multiplayer/4-Gameplay-Framework-+-networ)

[

## 📄️ Dedicated Server vs Listen Server

Dedicated Server

](https://ethyk.gitlab.io/unrealengine/multiplayer/5-Dedicated-Server-vs-Listen-Server)

[

## 📄️ Replication

What is 'Replication'?

](https://ethyk.gitlab.io/unrealengine/multiplayer/6-Replication)

[

## 📄️ Remote Procedure Calls

Other ways for Replication are so-called “RPC”s. Short form for “Remote Procedure Call”.

](https://ethyk.gitlab.io/unrealengine/multiplayer/7-Remote-Procedure-Calls)

[

## 📄️ Ownership

Ownership is something very important to understand. You already saw a table that contained entries like “Client-owned Actor”.

](https://ethyk.gitlab.io/unrealengine/multiplayer/8-Ownership)

[

## 📄️ Actor Relevancy and Priority

Relevancy

](https://ethyk.gitlab.io/unrealengine/multiplayer/8-Ownership)

[

## 📄️ Actor Role and RemoteRole

We have two more important properties for Actor replication.

](https://ethyk.gitlab.io/unrealengine/multiplayer/8-Ownership)

[

## 📄️ Traveling in Multiplayer

Non-/Seamless Travel

](https://cedric-neukirchen.net/docs/multiplayer-compendium/traveling-in-multiplayer)

[

## 📄️ How to Start a Multiplayer Game

The easiest way to start a Multiplayer Game is to set the Number of Players, in the Play Drop-Down Menu, to something higher than 1.

](https://ethyk.gitlab.io/unrealengine/multiplayer/8-Ownership)

[

## 📄️ Additional Resources

Here is a somewhat unordered list of additional resources that could help

](https://ethyk.gitlab.io/unrealengine/multiplayer/13-Additional-Resources)

<article ><a class="card" href="/multiplayer/1-introduction"><h2  title="Introduction">📄️ Introduction</h2><p  title="This Compendium should only be used with a base understanding of the Singleplayer Game Framework of Unreal Engine.">This Compendium should only be used with a base understanding of the Singleplayer Game Framework of Unreal Engine.</p></a></article>
